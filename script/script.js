/* ТЕОРЕТИЧНІ ПИТАННЯ

1. Опишіть своїми словами, що таке метод об'єкту
Метод об'єкта - це функція, яка належить об'єкту і є його властивістю.

2. Який тип даних може мати значення властивості об'єкта?
Значення властивості може бути будь-яким значенням, наприклад, рядком, числом, масивом, boolean, іншим об'єктом і навіть функцією.

3. Об'єкт це посилальний тип даних. Що означає це поняття?
Посилальний тип даних означає те що, кожен об'єкт для зменшення обсягу ваги при багатоповторному використанні в коді, 
має свій персональний адрес(посилання). При створені обєкта створюється відповідно його індивідуальний адрес, 
і в подальшому викорстанні цей адрес слугує безпосердньо як шлях до цього об'єкту.


ПРАКТИЧНІ ПИТАННЯ
1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. 
Викличте цей метод та результат виведіть в консоль.
*/

'use strict'

let product = {
    name: 'Гречка',
    price: 40,
    discount: 5,

    totalPrice () {
        return this.price - this.discount;
    }
}
console.log(product.totalPrice());

/* 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
наприклад "Привіт, мені 30 років".
Попросіть користувача ввести своє ім'я та вік
за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи). Результат виклику функції виведіть з допомогою alert.*/

function greeting(person) {
    return "Привіт, мені " + person.age + " років";
}

let name = prompt("Введіть ваше ім'я:");
let age = prompt("Введіть ваш вік:");

let user = {
    name: name,
    age: age
};

alert(greeting(user));


/*3.Опціональне. Завдання:
Реалізувати повне клонування об'єкта.*/

const userCard = {
    firstName: "Name",
    lastName: "Surname",
    age: 1,
    address: {
        country: "Ukraine",
        city: "Kyiv",
        postcode: "10001"
    },
    hobbies: ["hobby1", "hobby2", "hobby3"]
};
const serhiiCard = {};

for(let prop in userCard) {
    serhiiCard[prop] = userCard[prop];
}
serhiiCard.firstName = "Serhii";
serhiiCard.lastName = "Poslovskyi";
serhiiCard.age = 37;
serhiiCard.address = {
    country: "Ukraine",
    city: "Kyiv",
    postcode: "02098"
};
serhiiCard.hobbies = ["football", "swimming", "reading"];

console.log(serhiiCard);
